#!/bin/bash

function usage(){
	echo "Usage: $0 {<job name>} {<backup script's call>}"
    echo "IMPORTANT: you must put the script's call between quotes."
    echo "All whitespaces in the job name will be replaced by \'.\'."
}

[[ "$#" -ne 2 ]] && { echo "Illegal number of parameters!"; usage; exit 1; }

typeset -r job_name="${1// /.}" 
typeset -r basedir="/var/tmp"
typeset -r date_label="$(date +'%Y-%m-%d.%H-%M-%S')"
typeset -r backup_dir="${basedir}/backup.${date_label}.${job_name}"
mkdir -p ${backup_dir}

eval "$2 ${backup_dir}"

echo ""
echo "Compressing directory ${backup_dir}..."
zip -m -r "${backup_dir}.zip" ${backup_dir}
