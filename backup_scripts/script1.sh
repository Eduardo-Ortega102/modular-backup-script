#!/bin/bash

[[ ! "$#" -ge 1 ]] && { echo "You need to specify at least one parameter!"; exit 1; }

eval "typeset -r backup_dir=\$$#" # Get the last positional parameter.

[[ ! -d ${backup_dir} ]] && { echo "The directory \'${backup_dir}\' does not exist!"; exit 2; }

#                   #
#   your code here  #
#                   #

echo "First file saved inside '${backup_dir}'." > "${backup_dir}/example1.txt"
echo "Second file saved inside '${backup_dir}'." > "${backup_dir}/example2.txt"
echo "Third file saved inside '${backup_dir}'." > "${backup_dir}/example3.txt"
